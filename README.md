# Nakupovalni Listek 
Nakupovalni Listek je rešitev, ki uporabnikom omogoča poenostvljeno sestavljanje nakupovalnega listka s skeniranjem barkode izdelkov na mobilni aplikaciji za Android. Za razvoj aplikacije smo uporabili razvojno okolje Android Studio. Za razvojni jezik smo se odločili za javo z nekaterimi knjižnicami, vse podatke uporabnikov in izdelkov pa shranjujemo v podatkovno bazo mySQL, ki jo go gostuje DigitalOcean.

## Funkcionalnosti

-  Skeniranje barkode
-  Dodajanje nakupovalnega listka
-  Dodajanje izdelka na nakupovalni listek
-  Brisanje izdelka iz nakupovalnega listka
-  Prikaz trgovine, v kateri bo nakup najcenejši
-  Prikaz trgovine, v kateri je izdelek najcenejši


## Navodila za namestitev aplikacije

Naš projekt najdete na tej GitLab povezavi: https://gitlab.com/GalGustin/nakupovalni-listek

Zahteve za namestitev aplikacije: Nameščen Git in Android Studio

    1. Na zgoraj navadenem linku stisnemo 'Clone' in izberemo 'Clone with HTTPS'
    2. Prenesemo git in ustvarimo repozitorij v katero želimo klonirat projekt
    3. V ustvarjenem repozitoriju odpremo Windows Command Prompt in izvedemo komando 'git clone https://gitlab.com/GalGustin/nakupovalni-listek.git'
    4. Zaženemo Android Studio in v zgornjem levem kotu stisnemo File -> Open in izberemo mapo Frontend v kloniranem repozitoriju
    5. Na desni strani v Android Studio najdemo Device Manager, kjer stisnemo 'Physical' in nato 'Pair using Wi-Fi'
    6. Pozor! Na mobilnem telefonu morate imeti omogočeno Wireless debugging (brezžično odpravljanje napak), kjer stisnemo 'Seznanjanje naprave s kodo QR'
    7. Skeniramo QR-kodo na zaslonu in naš mobilen telefon je povezan z Android Studiom.
    8. V Android Studio stisnemo na 'Run app' v desnem zgornjem kotu


## Tehnološki sklad

-  Android Studio
-  Java
-  mySQL (DigitalOcean gostitelj)



## Avtorji

* [Gal Guštin](gal.gustin@student.um.si) _Frontend_
* [Bojan Petrovski](bojan.petrosvki@student.um.si) _Backend_
* [Klemen Kuri](klemen.kuri@student.um.si) _Backend_

## Kaj nismo uspeli naredit?
- Prikaz vse funkcionalnosti ki smo jih naredili
- Kreiranje nova aplikacija namenjena trgovinam
- Dodajanje sliko za vsak izdelek


