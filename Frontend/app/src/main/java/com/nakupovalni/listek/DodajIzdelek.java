package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nakupovalni.listek.Backend.dao.IzdelekDao;
import com.nakupovalni.listek.Backend.vao.Izdelek;

public class DodajIzdelek extends AppCompatActivity {

     private Button btnHome;
     private Button btnDodajIzdelek;
     private IzdelekDao izdelekDAO = new IzdelekDao();
     private Izdelek izdelek2 = new Izdelek();
     private TextView opisIzdelka;
     private TextView nazivIzdelka;
     private TextView barcodeText;
     private String st;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_izdelek);

        btnHome = findViewById(R.id.home1);
        btnDodajIzdelek = findViewById(R.id.novIzdelek);
        opisIzdelka = findViewById(R.id.editTextTextPersonName3);
        nazivIzdelka = findViewById(R.id.editNaziv);
        barcodeText = findViewById(R.id.CrtnaKoda);
        st = getIntent().getExtras().getString("barkoda1");
        barcodeText.setText(st);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(DodajIzdelek.this, Domov.class);
                startActivity(home);
            }
        });

        btnDodajIzdelek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                izdelek2.setBarcode(st);
                izdelek2.setNaziv(nazivIzdelka.getText().toString());
                izdelek2.setOpis(opisIzdelka.getText().toString());

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            izdelekDAO.dodajIzdelek(izdelek2);
                            Izdelek izdelek3 = izdelekDAO.najdiIzdelek(izdelek2.getBarcode());
                            Intent prikazIzdelka1 = new Intent(DodajIzdelek.this, PrikazIzdelka.class);
                            prikazIzdelka1.putExtra("izdelekId", izdelek3.getId());
                            prikazIzdelka1.putExtra("izdelekBarkoda", izdelek3.getBarcode());
                            prikazIzdelka1.putExtra("izdelekNaziv", izdelek3.getNaziv());
                            prikazIzdelka1.putExtra("izdelekOpis", izdelek3.getOpis());
                            startActivity(prikazIzdelka1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });



    }
}