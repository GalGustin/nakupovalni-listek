package com.nakupovalni.listek.Backend.vao;

public class TrgovinaIzdelek extends Izdelek {
 @Override
	public String toString() {
		return "TrgovinaIzdelek [izdelekID=" + izdelekID + ", cena=" + cena + "]";
	}
private int izdelekID;
 private double cena;
private TrgovinaIzdelek(int izdelekID, double cena,String naziv, String opis) {
	super(naziv,opis);
	this.izdelekID = izdelekID;
	this.cena = cena;
}
public int getIzdelekID() {
	return izdelekID;
}
public void setIzdelekID(int izdelekID) {
	this.izdelekID = izdelekID;
}
public double getCena() {
	return cena;
}
public void setCena(double cena) {
	this.cena = cena;
}
 
}
