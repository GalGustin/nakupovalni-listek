package com.nakupovalni.listek.Backend.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.nakupovalni.listek.Backend.vao.Izdelek;
import com.nakupovalni.listek.Backend.vao.Trgovina;

public class trgovinaDao {

	public Connection poveziBazo() {
		Connection conn = null;
		MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
		dataSource.setUser("myNewUser");
		dataSource.setPassword("myNewPassword");
		dataSource.setServerName("db-mysql-fra1-24226-do-user-11710591-0.b.db.ondigitalocean.com");
		dataSource.setPort(25060);
		dataSource.setDatabaseName("nakupovalniListek");

		try {
			conn = dataSource.getConnection();
			System.out.println("SUCCESS");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("FAIL");
			e.printStackTrace();
		}
		return conn;
	}


	public void dodajTrgovino(Trgovina trgovina) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		 String sql = "INSERT INTO Trgovina (idTrgovina, naziv) VALUES (?,?)";
         PreparedStatement prest=conn.prepareStatement(sql);
         prest.setInt(1, trgovina.getId());
         prest.setString(2, trgovina.getNaziv());
		 prest.executeUpdate();
    }
	public Trgovina najdiTrgovina(String naziv) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		Trgovina vrni=null;
		String sql="select * from Trgovina where naziv=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, naziv);
		ResultSet result=prest.executeQuery();
		 while (result.next()) {
		int id=result.getInt("idTrgovina");
		String na= result.getString("naziv");
		Trgovina novaTrgovina=new Trgovina(id,na);
		vrni=novaTrgovina;
		 }
		 
		return vrni;
	}
}
