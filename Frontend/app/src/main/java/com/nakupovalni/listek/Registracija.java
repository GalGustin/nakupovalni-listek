package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nakupovalni.listek.Backend.dao.UporabnikDao;
import com.nakupovalni.listek.Backend.vao.Uporabnik;

import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class Registracija extends AppCompatActivity {

    Button btnHome;
    Button btnRegistriraj;
    EditText edtUporabniskoIme;
    EditText edtGeslo;
    EditText edtMail;
    UporabnikDao uporabnikDAO = new UporabnikDao();
    Uporabnik uporabnik = new Uporabnik();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija);

        btnHome = findViewById(R.id.home1);
        btnRegistriraj = findViewById(R.id.registracija);
        edtUporabniskoIme = findViewById(R.id.editTextTextPersonName);
        edtGeslo = findViewById(R.id.editTextTextPassword);
        edtMail = findViewById(R.id.editTextTextEmailAddress);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Registracija.this, Domov.class);
                startActivity(home);
            }
        });
        btnRegistriraj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pbkdf2PasswordEncoder pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder();
                String pbkdf2CryptedPassword = pbkdf2PasswordEncoder.encode(edtGeslo.getText().toString());

                uporabnik.setPassword(pbkdf2CryptedPassword);
                //System.out.println("GESLO: " + pbkdf2CryptedPassword);
                uporabnik.setUporabniskoIme(edtUporabniskoIme.getText().toString());
                //System.out.println("MAIL: " + uporabnik.getUporabniskoIme());
                uporabnik.setEmail(edtMail.getText().toString());
                //System.out.println("MAIL: " + uporabnik.getEmail() + " " + uporabnik.getEmail().length());
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            uporabnikDAO.dodajUporabnika(uporabnik);
                            Intent prijava = new Intent(Registracija.this, Prijava.class);
                            startActivity(prijava);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });
    }
}