package com.nakupovalni.listek;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nakupovalni.listek.Backend.vao.NakupovalniListek;

import java.util.ArrayList;
import java.util.List;

public class NakupovalniListekAdapter extends ArrayAdapter<NakupovalniListek> {

    private Context mContext;
    private int mResource;

    public NakupovalniListekAdapter(Context context, int resource, List<NakupovalniListek> objects) {
        super(context, resource, objects);
        this.mResource=resource;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String naziv = getItem(position).getNazivListka();

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        TextView tvNaziv = (TextView) convertView.findViewById(R.id.textView9);

        tvNaziv.setText(naziv);

        return convertView;
    }
}
