package com.nakupovalni.listek.Backend.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.nakupovalni.listek.Backend.vao.Izdelek;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;
import com.nakupovalni.listek.Backend.vao.Trgovina;
import com.nakupovalni.listek.Backend.vao.TrgovinaIzdelek;
import com.nakupovalni.listek.Backend.vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.xml.transform.Result;

public class KosaricaDao {

	public Connection poveziBazo() {
		Connection conn = null;
		MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
		dataSource.setUser("myNewUser");
		dataSource.setPassword("myNewPassword");
		dataSource.setServerName("db-mysql-fra1-24226-do-user-11710591-0.b.db.ondigitalocean.com");
		dataSource.setPort(25060);
		dataSource.setDatabaseName("nakupovalniListek");

		try {
			conn = dataSource.getConnection();
			System.out.println("SUCCESS");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("FAIL");
			e.printStackTrace();
		}
		return conn;
	}


	public void najboljProdanIzdelek() throws SQLException {
		Connection conn = poveziBazo();
		String sql="create or replace view Izdelke as select naziv,opis,idIzdelek, COUNT(Kosarica.IzdelekID) as ProduktVKosarici"
				+ " from Kosarica,Izdelek "
				+ "where IzdelekID=idIzdelek "
				+ "GROUP BY IzdelekID;";
		String sql2="SELECT * from Izdelke where ProduktVKosarici = ( SELECT MAX(ProduktVKosarici) from Izdelke)";
		PreparedStatement prest= conn.prepareStatement(sql);
		boolean rezultat=prest.execute();
		PreparedStatement prest2=conn.prepareStatement(sql2);
		ResultSet result=prest2.executeQuery();
		
		 if (result.next()) {
		int id=result.getInt("idIzdelek");
		String naziv= result.getString("naziv");
		String opis= result.getString("opis");
		int kolicina=result.getInt("ProduktVKosarici");
		System.out.println("Najbolj prodani produkt je: "+naziv+" Opis: "+opis+"\n in je bil na skupno: "+kolicina+"-- kosaric");
		 }
	}
}

