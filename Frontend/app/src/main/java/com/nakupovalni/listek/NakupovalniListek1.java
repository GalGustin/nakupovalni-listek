package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.nakupovalni.listek.Backend.dao.NakupovalniListekDao;
import com.nakupovalni.listek.Backend.vao.Kosarica;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;

import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import java.io.Serializable;
import java.util.ArrayList;

public class NakupovalniListek1 extends AppCompatActivity {

    private ListView mListView;
    SharedPreferences sharedpreferences;
    String uporabniskoIme;
    ArrayList<NakupovalniListek> nakupListek;
    ArrayList<Kosarica> nakupListekIzdelek;
    NakupovalniListekDao nakupovalniListekDao = new NakupovalniListekDao();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nakupovalni_listek);
        mListView = findViewById(R.id.listNakupListek);
        sharedpreferences = getSharedPreferences("uporabnik123", Context.MODE_PRIVATE);
        uporabniskoIme = getIntent().getExtras().getString("uporabniskoIme12");
        nakupListek = (ArrayList<NakupovalniListek>) getIntent().getSerializableExtra("arrayListkov");

        NakupovalniListekAdapter nakupAdapter = new NakupovalniListekAdapter(this, R.layout.adapter_view_nakupovalni, nakupListek);
        mListView.setAdapter(nakupAdapter);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            Intent intent = new Intent(NakupovalniListek1.this,NakupovalniListekIzdelek.class);
                            System.out.println(nakupListek.get(i).getNazivListka());
                            nakupListekIzdelek = nakupovalniListekDao.najdiProdukteNaListku(nakupListek.get(i).getNazivListka());
                            intent.putExtra("name", nakupListek.get(i).getNazivListka());
                            intent.putExtra("arrayListekIzdelek", (Serializable) nakupListekIzdelek);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });
    }
}