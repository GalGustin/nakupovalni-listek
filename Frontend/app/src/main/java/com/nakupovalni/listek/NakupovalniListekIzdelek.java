package com.nakupovalni.listek;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.nakupovalni.listek.Backend.vao.Kosarica;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;

import java.util.ArrayList;

public class NakupovalniListekIzdelek extends AppCompatActivity {

    private ListView nListView;
    SharedPreferences sharedpreferences;
    String uporabniskoIme;
    ArrayList<Kosarica> nakupListekIzdelek;
    String nakupovalniListekNaziv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nakupovalni_listek_izdelek);
        nListView = findViewById(R.id.nakupListekIzdelek);
        nakupovalniListekNaziv = getIntent().getExtras().getString("name");
        System.out.println(nakupovalniListekNaziv);
        sharedpreferences = getSharedPreferences("uporabnik123", Context.MODE_PRIVATE);
        uporabniskoIme = getIntent().getExtras().getString("name");
        nakupListekIzdelek = (ArrayList<Kosarica>) getIntent().getSerializableExtra("arrayListekIzdelek");

        NakupovalniListekIzdelekAdapter nakupIzdelekAdapter = new NakupovalniListekIzdelekAdapter(this, R.layout.adapter_view_layout, nakupListekIzdelek);
        nListView.setAdapter(nakupIzdelekAdapter);

    }
}