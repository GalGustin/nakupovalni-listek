package com.nakupovalni.listek.Backend.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.nakupovalni.listek.Backend.vao.Izdelek;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;
import com.nakupovalni.listek.Backend.vao.Trgovina;
import com.nakupovalni.listek.Backend.vao.TrgovinaIzdelek;
import com.nakupovalni.listek.Backend.vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class IzdelekDao {

	public Connection poveziBazo() {
		Connection conn = null;
		MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
		dataSource.setUser("myNewUser");
		dataSource.setPassword("myNewPassword");
		dataSource.setServerName("db-mysql-fra1-24226-do-user-11710591-0.b.db.ondigitalocean.com");
		dataSource.setPort(25060);
		dataSource.setDatabaseName("nakupovalniListek");

		try {
			conn = dataSource.getConnection();
			System.out.println("SUCCESS");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("FAIL");
			e.printStackTrace();
		}
		return conn;
	}

	public void dodajIzdelek(Izdelek izdelek) throws SQLException {
		 Connection conn = poveziBazo();
		 String sql = "INSERT INTO Izdelek (barcode, naziv, opis) VALUES (?,?,?)";
         PreparedStatement prest=conn.prepareStatement(sql);
         prest.setString(1, izdelek.getBarcode());
         prest.setString(2, izdelek.getNaziv());
         prest.setString(3, izdelek.getOpis());
		 prest.executeUpdate();
    }
	public Izdelek najdiIzdelek(String barcode) throws SQLException {
		Connection conn = poveziBazo();
		Izdelek vrni=null;
		String sql="select * from Izdelek where barcode=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, barcode);
		ResultSet result=prest.executeQuery();
		while (result.next()) {
			int id=result.getInt("idIzdelek");
			String na= result.getString("naziv");
			String opis= result.getString("opis");
			Izdelek novIzdelek=new Izdelek(id, barcode,na,opis);
			vrni=novIzdelek;
		}
		return vrni;
	}
	
	public boolean preveriIzdelek(String barcode) throws SQLException {
		Connection conn = poveziBazo();
		String sql="select * from Izdelek where barcode=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, barcode);
		ResultSet result=prest.executeQuery();
		 if (result.next()) {
			 return true;
		}
		 else return false;
	}
}


