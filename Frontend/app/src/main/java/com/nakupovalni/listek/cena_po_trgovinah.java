package com.nakupovalni.listek;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nakupovalni.listek.databinding.ActivityCenaPoTrgovinahBinding;

import java.util.HashMap;
import java.util.Map;

public class cena_po_trgovinah extends AppCompatActivity {

    private Button btnHome;
    private TextView prikaz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cena_po_trgovinah);

        btnHome = findViewById(R.id.domov);
        prikaz = findViewById(R.id.cenePoTrgovinah);

        Intent intent = getIntent();
        HashMap<String, Double> hashMap = (HashMap<String, Double>)intent.getSerializableExtra("map");
        String cene="";
        for (Map.Entry<String, Double> e : hashMap.entrySet()){
            cene+="Cena v "+e.getKey()+" je: "+e.getValue()+"\n";
            System.out.println("Key: " + e.getKey()
                    + " Value: " + e.getValue());}
        prikaz.setText(cene);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(cena_po_trgovinah.this, Domov.class);
                startActivity(home);
            }

        });
    }
}