package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.nakupovalni.listek.Backend.dao.*;
import com.nakupovalni.listek.Backend.vao.*;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

public class PrikazIzdelka extends AppCompatActivity {

    private Button btnHome;
    private Button btnNov;
    private Button btnPrikaz;
    private TextView nazivIzdelka;
    private TextView opisIzdelka;
    private TextView najnizjaCena;
    private IzdelekDao izdelekDAO = new IzdelekDao();
    private IzdelekTrgovinaDao ITD = new IzdelekTrgovinaDao();
    private Izdelek izdelek1 = new Izdelek();
    private TextView barcodeText12;
    private String izdelekBarkoda;
    private int izdelekId;
    private String izdelekNaziv;
    private String izdelekOpis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prikaz_izdelka);

        btnHome = findViewById(R.id.home1);
        btnNov = findViewById(R.id.novIzdelek);
        btnPrikaz= findViewById(R.id.cenaPoTrgovine);
        opisIzdelka = findViewById(R.id.opisIzdelka);
        nazivIzdelka = findViewById(R.id.nazivIzdelka);
        najnizjaCena = findViewById(R.id.textView5);

        izdelekId = getIntent().getExtras().getInt("izdelekId");
        izdelekBarkoda = getIntent().getExtras().getString("izdelekBarkoda");
        izdelekNaziv = getIntent().getExtras().getString("izdelekNaziv");
        izdelekOpis = getIntent().getExtras().getString("izdelekOpis");

        izdelek1 = new Izdelek(izdelekId, izdelekBarkoda, izdelekNaziv, izdelekOpis);
        opisIzdelka.setText(izdelek1.getOpis());
        nazivIzdelka.setText(izdelek1.getNaziv());

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(PrikazIzdelka.this, Domov.class);
                startActivity(home);
            }

        });

        btnNov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nov = new Intent(PrikazIzdelka.this, Scanner.class);
                startActivity(nov);
            }
        });
        btnPrikaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            Intent prikazCene = new Intent(PrikazIzdelka.this, cena_po_trgovinah.class);
                            HashMap<String,Double> cenePoTrgovinah= ITD.ceneIzdelkaVseTrgovine(izdelekBarkoda);
                            prikazCene.putExtra("map", (Serializable) cenePoTrgovinah);
                            startActivity(prikazCene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });
    }

}