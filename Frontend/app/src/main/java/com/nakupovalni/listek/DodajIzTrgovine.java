package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nakupovalni.listek.Backend.dao.IzdelekDao;
import com.nakupovalni.listek.Backend.dao.IzdelekTrgovinaDao;
import com.nakupovalni.listek.Backend.dao.UporabnikDao;
import com.nakupovalni.listek.Backend.vao.Izdelek;
import com.nakupovalni.listek.Backend.vao.Uporabnik;

import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class DodajIzTrgovine extends AppCompatActivity {
    private Button btnHome;
    private Button btnDodaj;
    private EditText edtNaziv;
    private EditText edtTrgovina;
    private EditText edtBarcode;
    private EditText edtCena;
    private EditText edtOpis;

    private String st;

    IzdelekTrgovinaDao izdelekTrgovinaDao = new IzdelekTrgovinaDao();
    IzdelekDao izdelekDao = new IzdelekDao();
    Izdelek izdelek12;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_iz_trgovine);
        btnHome = findViewById(R.id.button9);
        btnDodaj = findViewById(R.id.novIzdelek);
        edtNaziv = findViewById(R.id.editNaziv);
        edtTrgovina = findViewById(R.id.editTrgovina);
        edtBarcode = findViewById(R.id.BarcodeIzdelka);
        edtCena = findViewById(R.id.editCena);
        edtOpis = findViewById(R.id.editTrgovina2);

        st = getIntent().getExtras().getString("barkoda1");
        edtBarcode.setText(st);

        System.out.println(st);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(DodajIzTrgovine.this, Domov.class);
                startActivity(home);
            }
        });
        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            izdelek12 = new Izdelek(st, edtNaziv.getText().toString(), edtOpis.getText().toString());
                            izdelekDao.dodajIzdelek(izdelek12);
                            //System.out.println("Test1");
                            izdelekTrgovinaDao.dodajIzTrgovino(izdelek12.getNaziv(),edtTrgovina.getText().toString(),st ,Double.parseDouble(edtCena.getText().toString()));
                            //System.out.println("Test2");
                            Intent prikazIzdelka1 = new Intent(DodajIzTrgovine.this, PrikazIzdelka.class);
                            prikazIzdelka1.putExtra("izdelekId", izdelek12.getId());
                            prikazIzdelka1.putExtra("izdelekBarkoda", izdelek12.getBarcode());
                            prikazIzdelka1.putExtra("izdelekNaziv", izdelek12.getNaziv());
                            prikazIzdelka1.putExtra("izdelekOpis", izdelek12.getOpis());
                            startActivity(prikazIzdelka1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });

    }
}