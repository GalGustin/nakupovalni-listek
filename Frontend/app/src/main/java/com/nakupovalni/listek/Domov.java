package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Domov extends AppCompatActivity {

    Button btnReg, btnLogin, btnScan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domov);

        btnLogin = findViewById(R.id.button3);
        btnReg = findViewById(R.id.button4);
        btnScan = findViewById(R.id.button5);




        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg = new Intent(Domov.this,Registracija.class);
                startActivity(reg);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(Domov.this,Prijava.class);
                startActivity(login);
            }
        });
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scan = new Intent(Domov.this, Scanner.class);
                startActivity(scan);
            }
        });

    }
}