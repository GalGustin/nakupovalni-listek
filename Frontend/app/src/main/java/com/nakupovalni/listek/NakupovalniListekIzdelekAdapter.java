package com.nakupovalni.listek;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nakupovalni.listek.Backend.vao.Kosarica;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;

import java.util.ArrayList;
import java.util.List;

public class NakupovalniListekIzdelekAdapter extends ArrayAdapter<Kosarica> {

    private Context nContext;
    private int nResource;

    public NakupovalniListekIzdelekAdapter(Context context, int resource, ArrayList<Kosarica> objects) {
        super(context, resource, objects);
        this.nResource=resource;
        this.nContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String nazivIzdelka = getItem(position).getNaziv();
        Double cenaIzdelka = getItem(position).getCena();

        LayoutInflater inflater = LayoutInflater.from(nContext);
        convertView = inflater.inflate(nResource,parent,false);

        TextView tv1Naziv = (TextView) convertView.findViewById(R.id.textView3);
        TextView tv1Cena = (TextView) convertView.findViewById(R.id.textView8);

        tv1Naziv.setText(nazivIzdelka);

        return convertView;
    }
}
