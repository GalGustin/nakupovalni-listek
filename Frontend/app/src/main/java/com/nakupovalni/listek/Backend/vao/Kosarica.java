package com.nakupovalni.listek.Backend.vao;

public class Kosarica {
	private double cena;
	private String naziv;
	public Kosarica(String naziv, double cena) {
		super();
		this.setNaziv(naziv);
		this.setCena(cena);
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	@Override
	public String toString() {
		return "Kosarica [Naziv=" + naziv + ", cena=" + cena + "]";
	}
}
