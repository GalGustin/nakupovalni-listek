package com.nakupovalni.listek.Backend.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.nakupovalni.listek.Backend.vao.Izdelek;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;
import com.nakupovalni.listek.Backend.vao.Trgovina;
import com.nakupovalni.listek.Backend.vao.TrgovinaIzdelek;
import com.nakupovalni.listek.Backend.vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class UporabnikDao {

	public Connection poveziBazo() {
		Connection conn = null;
		MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
		dataSource.setUser("myNewUser");
		dataSource.setPassword("myNewPassword");
		dataSource.setServerName("db-mysql-fra1-24226-do-user-11710591-0.b.db.ondigitalocean.com");
		dataSource.setPort(25060);
		dataSource.setDatabaseName("nakupovalniListek");

		try {
			conn = dataSource.getConnection();
			System.out.println("SUCCESS");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("FAIL");
			e.printStackTrace();
		}
		return conn;
	}

	//dodajanje uporabnika, ce ne obstaja
	public void dodajUporabnika(Uporabnik uporabnik) throws SQLException {
		Connection conn = poveziBazo();
		if(najdiUporabnika(uporabnik.getUporabniskoIme())==false) {
			Statement st=conn.createStatement();
			st.executeUpdate("INSERT INTO Uporabnik (uporabniskoIme, geslo, mail) "
                 +"VALUES ('"+uporabnik.getUporabniskoIme()+"'"  +", '"+ uporabnik.getPassword() +"'"  +", '"+uporabnik.getEmail()+ "')");
		}
	}
	
	//brisanje uporabnika
	public void brisiUporabnika(String uporabniskoIme) throws SQLException {
		Connection conn = poveziBazo();

        String sql = "DELETE FROM Uporabnik WHERE uporabniskoIme=?";
        PreparedStatement prest = conn.prepareStatement(sql);
        prest.setString(1, uporabniskoIme);
        
        int val = prest.executeUpdate();
        System.out.println("BRISANJE�UPORABNIKA: " +uporabniskoIme);
    }
	
	//preveri ce uporabnik obstaja in vrne true, ce obstaja ali false, ce ne obstaja
	public boolean najdiUporabnika(String uporabniskoIme)throws SQLException {
		System.out.println("ISKANJE UPORABNIKA: " + uporabniskoIme);
		
		Connection conn = poveziBazo();
		String sql = "SELECT (uporabniskoIme) FROM Uporabnik WHERE uporabniskoIme=?";
		PreparedStatement prest=conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		prest.setString(1, uporabniskoIme);
		ResultSet rs = prest.executeQuery();
		rs.last();

	    if(rs.getRow() > 0) {
	    	return true;
	    }else {
	    	return false;
	    }   
    }

	//ustvari objekt uporabnika
	public Uporabnik najdiUporabnika1(String uporabniskoIme)throws SQLException {
		System.out.println("ISKANJE UPORABNIKA: " + uporabniskoIme);
		Uporabnik vrni=null;
		Connection conn = poveziBazo();
		String sql = "SELECT * FROM Uporabnik WHERE uporabniskoIme=?";
		PreparedStatement prest=conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		prest.setString(1, uporabniskoIme);
		ResultSet rs = prest.executeQuery();
		while (rs.next()) {
			int id=rs.getInt("idUporabnik");
			String uporabnisko= rs.getString("uporabniskoIme");
			String geslo = rs.getString("geslo");
			String mail = rs.getString("mail");
			Uporabnik novUporabnik=new Uporabnik(id, uporabnisko,geslo,mail);
			vrni=novUporabnik;
		}
		return vrni;
	}
	public void mojIzdelek(Uporabnik uporabnik) throws SQLException {
		Connection conn = poveziBazo();
		String sql="create or replace view Izdelke as select naziv,opis,idIzdelek,uporabniskoime, COUNT(Kosarica.IzdelekID) as ProduktVKosarici from Kosarica,Izdelek,Uporabnik,NakupovalniListek where IzdelekID=idIzdelek and nakupovalnilistek_idnakupovalnilistek=idnakupovalnilistek and uporabnik_iduporabnik=iduporabnik\r\n"
				+ "and iduporabnik=?\r\n"
				+ "GROUP BY IzdelekID,idUporabnik;";
		String sql2="SELECT * from Izdelke where ProduktVKosarici = ( SELECT MAX(ProduktVKosarici) from Izdelke)";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, uporabnik.getId());;
		boolean rezultat=prest.execute();
		PreparedStatement prest2=conn.prepareStatement(sql2);
		ResultSet result=prest2.executeQuery();
		
		 if (result.next()) {
		int id=result.getInt("idIzdelek");
		String naziv= result.getString("naziv");
		String opis= result.getString("opis");
		int kolicina=result.getInt("ProduktVKosarici");
		System.out.println("Najbolj prodani produkt je: "+naziv+" Opis: "+opis+"\n in je bil na skupno: "+kolicina+"-- kosaric");
		 }
	}
	public Uporabnik najdiUporabnikID(String uporabniskoIme)throws SQLException {		
		Connection conn = poveziBazo();
		String sql = "SELECT * FROM Uporabnik WHERE uporabniskoIme=?";
		PreparedStatement prest=conn.prepareStatement(sql);
		prest.setString(1, uporabniskoIme);
		Uporabnik uporabnik=new Uporabnik();
		ResultSet rs = prest.executeQuery();
		  while(rs.next()) {
			  String ui=rs.getString("uporabniskoIme");
			  String geslo=rs.getString("geslo");
			  String mail=rs.getString("mail");
			  int id = rs.getInt("idUporabnik");
			  Uporabnik temp=new Uporabnik(id,ui,geslo,mail);
			  uporabnik=temp;
		  }
		  return uporabnik;
    }
	
}
