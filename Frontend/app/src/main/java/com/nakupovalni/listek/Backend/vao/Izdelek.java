package com.nakupovalni.listek.Backend.vao;

public class Izdelek {
	@Override
	public String toString() {
		return "Izdelek [id=" + id + ", naziv=" + naziv + ", opis=" + opis + "]";
	}
	private int id;
	private String naziv;
	private String opis;
	private String barcode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public Izdelek() {
	}

	public Izdelek(String naziv, String opis) {
		super();
		this.naziv = naziv;
		this.opis = opis;
	}
	public Izdelek(String barcode, String naziv, String opis) {
		super();
		this.barcode = barcode;
		this.naziv = naziv;
		this.opis = opis;
	}
	public Izdelek(int id, String barcode, String naziv, String opis) {
		super();
		this.id = id;
		this.barcode = barcode;
		this.naziv = naziv;
		this.opis = opis;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	
}
