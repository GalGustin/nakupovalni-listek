package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class IzbiraDodajanja extends AppCompatActivity {

    private Button btnHome, btnTrgovina, btnDoma;
    private String st;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izbira_dodajanja);
        btnHome = findViewById(R.id.home1);
        btnTrgovina = findViewById(R.id.trgovina);
        btnDoma = findViewById(R.id.doma);
        st = getIntent().getExtras().getString("barkoda1");

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(IzbiraDodajanja.this, Domov.class);
                startActivity(home);
            }
        });

        btnTrgovina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg = new Intent(IzbiraDodajanja.this,DodajIzTrgovine.class);
                reg.putExtra("barkoda1", st);
                startActivity(reg);
            }
        });

        btnDoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg = new Intent(IzbiraDodajanja.this,DodajIzdelek.class);
                reg.putExtra("barkoda1", st);
                startActivity(reg);
            }
        });
    }
}