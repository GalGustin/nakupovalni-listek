package com.nakupovalni.listek.Backend.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.nakupovalni.listek.Backend.vao.Izdelek;
import com.nakupovalni.listek.Backend.vao.Kosarica;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;
import com.nakupovalni.listek.Backend.vao.Trgovina;
import com.nakupovalni.listek.Backend.vao.TrgovinaIzdelek;
import com.nakupovalni.listek.Backend.vao.Uporabnik;
import com.nakupovalni.listek.Backend.dao.UporabnikDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class NakupovalniListekDao {
	public UporabnikDao uporabnikDao=new UporabnikDao();
	public Connection poveziBazo() {
		Connection conn = null;
		MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
		dataSource.setUser("myNewUser");
		dataSource.setPassword("myNewPassword");
		dataSource.setServerName("db-mysql-fra1-24226-do-user-11710591-0.b.db.ondigitalocean.com");
		dataSource.setPort(25060);
		dataSource.setDatabaseName("nakupovalniListek");

		try {
			conn = dataSource.getConnection();
			System.out.println("SUCCESS");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("FAIL");
			e.printStackTrace();
		}
		return conn;
	}


	//Dodajanje listka
	public void dodajNakupovalniListek(NakupovalniListek nakupovalniListek) throws SQLException {
		Connection conn = poveziBazo();
		
		String sql = "INSERT INTO NakupovalniListek (iUporabnik_idUporabnik, skupna_cena) VALUES (?,?, ?)";
        PreparedStatement prest=conn.prepareStatement(sql);
        prest.setInt(1, nakupovalniListek.getUporabnikID());
		prest.setFloat(2, (float) nakupovalniListek.getSkupnaCena());
		
		prest.executeUpdate();
		
		System.out.println("DODAJANJE LISTKA: " + nakupovalniListek.getId());
    }
	
	//Brisajne listka
	public void brisiNakupovalniListek(int id) throws SQLException {
		Connection conn = poveziBazo();
		
		String sql = "DELETE FROM NakupovalniListek WHERE idNakupovalniListek=?";
        PreparedStatement prest = conn.prepareStatement(sql);
        prest.setInt(1, id);
        
        prest.executeUpdate();
        System.out.println("BRISANJE LISTKA: " + id);
	}
	public double skupnaCenaListka(Trgovina tr) throws SQLException {
		Connection conn = poveziBazo();
		double cena=0;
		
		String sql= "select MIN(IzdelekVTrgovini.cena), COUNT(kolicina) from Kosarica,Izdelek,IzdelekVTrgovini"
				+ " where IzdelekID=idIzdelek"
				+ " and IzdelekID=Izdelek_idIzdelek and Izdelek_idIzdelek=idIzdelek"
				+ " and trgovina_idTrgovina=? group by idIzdelek";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,tr.getId());
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			double cenaIzdelka=result.getDouble(1)*result.getInt(2);
			cena+=cenaIzdelka;
		}
		System.out.println("Cena za celotno kosarico je: "+cena);
		
		return cena;
		
	}
	
	public double skupnaCenaListka(String nazivTrgovina) throws SQLException {
		Connection conn = poveziBazo();
		double cena=0;
		
		//s tem pridobim idTrgovine iz baze, ki ga potem v spodnjem sqlu uporabim za skupno ceno
		String sql1 = "select idTrgovina from Trgovina where naziv=?";
		PreparedStatement prest1= conn.prepareStatement(sql1);
		prest1.setString(1, nazivTrgovina);
		ResultSet result1 = prest1.executeQuery();
		int id = 0;
		if (result1.next()) {
            id = result1.getInt("idTrgovina");
            //System.out.println(count);
        }
		//System.out.println("ID" + id);
		
		String sql= "select MIN(IzdelekVTrgovini.cena), COUNT(kolicina) from Kosarica,Izdelek,IzdelekVTrgovini"
				+ " where IzdelekID=idIzdelek"
				+ " and IzdelekID=Izdelek_idIzdelek and Izdelek_idIzdelek=idIzdelek"
				+ " and trgovina_idTrgovina=? group by idIzdelek";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,id);
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			double cenaIzdelka=result.getDouble(1)*result.getInt(2);
			cena+=cenaIzdelka;
		}
		System.out.println("Cena za celotno kosarico v trgovini " + nazivTrgovina + " je: "+cena);
		
		return cena;
		
	}
	public Kosarica najcenejsaTrgovina() throws SQLException{
		Connection conn = poveziBazo();
		
		ArrayList<Double> arrCene = new ArrayList<Double>();
		ArrayList<String> arrNazivi = new ArrayList<String>();
		
		String sql= "select * from Trgovina group by Trgovina.idTrgovina";
		PreparedStatement prest= conn.prepareStatement(sql);
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			String naziv=result.getString("naziv");
			//skupnaCenaListka(naziv);
			arrCene.add(skupnaCenaListka(naziv));
			arrNazivi.add(naziv);
		}
		
		double najcenejsaCena=arrCene.get(0);
		String nazivTrgovine = "";
		for(int i=0; i<arrCene.size(); i++) {
			if (arrCene.get(i)<najcenejsaCena) {
				najcenejsaCena=arrCene.get(i);
				nazivTrgovine=arrNazivi.get(i);
			}
		}
		//System.out.println("Trgovina: " + naziv + " Cena: "+ najcenejsaCena);
		Kosarica najcenejsaKosarica = new Kosarica(nazivTrgovine, najcenejsaCena);
		return najcenejsaKosarica;
	}
	public ArrayList<NakupovalniListek> najdiUporabniskeListke(String uporabniskoIme) throws SQLException {
		Connection conn = poveziBazo();
		UporabnikDao UD=new UporabnikDao();
		Uporabnik uporabnik=UD.najdiUporabnikID(uporabniskoIme);
		ArrayList<NakupovalniListek> uporabniskeListke = new ArrayList<NakupovalniListek>();
		NakupovalniListek nakupListek;
		String sql= "select * from NakupovalniListek where Uporabnik_idUporabnik=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,uporabnik.getId());
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			nakupListek = null;
			int id=result.getInt("idNakupovalniListek");
			int na= result.getInt("Uporabnik_idUporabnik");
			double opis= result.getDouble("skupnaCena");
			String imeListka=result.getString("listekNaziv");
			nakupListek = new NakupovalniListek(id, na, opis, imeListka);
			uporabniskeListke.add(nakupListek);
		}
		return uporabniskeListke;
	}

	public NakupovalniListek najdiNakupovalniListek(String naziv) throws SQLException {
		Connection conn = poveziBazo();
		NakupovalniListek vrni=null;
		String sql="select * from NakupovalniListek where listekNaziv=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, naziv);
		ResultSet result=prest.executeQuery();
		while (result.next()) {
			int id=result.getInt("idNakupovalniListek");
			int uporabnikID=result.getInt("Uporabnik_idUporabnik");
			double skupnaCena=result.getDouble("skupnaCena");
			String na= result.getString("listekNaziv");
			NakupovalniListek nakupovalniListek=new NakupovalniListek(id,uporabnikID,skupnaCena,na);
			vrni=nakupovalniListek;
		}
		return vrni;
	}
	public ArrayList<Kosarica> najdiProdukteNaListku (String naziv) throws SQLException{
		Connection conn = poveziBazo();
		NakupovalniListek nakupovalniListek= najdiNakupovalniListek(naziv);

		Kosarica vrni = null;
		ArrayList<Kosarica> produkteNaListku = new ArrayList<Kosarica>();

		String sql= "SELECT naziv, MIN(cena) from Kosarica,NakupovalniListek,Izdelek, IzdelekVTrgovini WHERE idNakupovalniListek=nakupovalnilistek_idNakupovalniListek AND IzdelekID=idIzdelek AND IzdelekID=Izdelek_idIzdelek and listekNaziv=? GROUP BY naziv";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,nakupovalniListek.getId());
		ResultSet result= prest.executeQuery();

		while(result.next()) {
			double skupnaCena=result.getDouble("cena");
			String na= result.getString("naziv");
			Kosarica kosarica =new Kosarica(na, skupnaCena);
			produkteNaListku.add(kosarica);
		}
		return produkteNaListku;
	}


}