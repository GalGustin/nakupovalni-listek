package com.nakupovalni.listek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.nakupovalni.listek.Backend.dao.NakupovalniListekDao;
import com.nakupovalni.listek.Backend.dao.UporabnikDao;
import com.nakupovalni.listek.Backend.vao.NakupovalniListek;
import com.nakupovalni.listek.Backend.vao.Uporabnik;

import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import java.io.Serializable;
import java.util.ArrayList;

public class Prijava extends AppCompatActivity {

    Button btnHome;
    Button btnPrijava;
    EditText edtPrijavaGeslo;
    EditText edtPrijavaUporabniskoIme;
    Uporabnik uporabnik = new Uporabnik();
    UporabnikDao uporabnikDAO = new UporabnikDao();
    NakupovalniListekDao nakupListekDao = new NakupovalniListekDao();
    SharedPreferences sharedpreferences;
    public static final String UporabnikSession = "uporabnik123";
    public static final int idUporabnikaSession=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prijava);
        btnHome = findViewById(R.id.home1);
        btnPrijava = findViewById(R.id.prijava);
        edtPrijavaUporabniskoIme = findViewById(R.id.editNaziv);
        edtPrijavaGeslo = findViewById(R.id.editTextTextPassword2);
        sharedpreferences = getSharedPreferences("uporabnik123", Context.MODE_PRIVATE);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Prijava.this, Domov.class);
                startActivity(home);
            }
        });

        btnPrijava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {

                            //to mi zdaj naredi uporabnika, glede na podatke iz baze, zdaj pa preveri če je geslo pravilno sploh???
                            //potem pa daj session
                            uporabnik = uporabnikDAO.najdiUporabnika1(edtPrijavaUporabniskoIme.getText().toString());
                            Pbkdf2PasswordEncoder pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder();
                            boolean passwordIsValid = pbkdf2PasswordEncoder.matches(edtPrijavaGeslo.getText().toString(), uporabnik.getPassword());
                            if(passwordIsValid){
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putInt(String.valueOf(idUporabnikaSession), uporabnik.getId());
                                editor.putString(UporabnikSession, uporabnik.getUporabniskoIme());
                                System.out.println(uporabnik.getId());
                                ArrayList<NakupovalniListek> arrayNakupovalnihListkov = nakupListekDao.najdiUporabniskeListke(uporabnik.getUporabniskoIme());
                                //Prijava je uspešna, da te na novo stran

                                Intent account = new Intent(Prijava.this, NakupovalniListek1.class);
                                account.putExtra("uporabniskoIme12", uporabnik.getUporabniskoIme());
                                account.putExtra("arrayListkov", (Serializable) arrayNakupovalnihListkov);
                                startActivity(account);
                            }
                            else{
                                //GESLO JE NAPAČNO MSG
                            }
                            System.out.println(passwordIsValid + " " + edtPrijavaGeslo.getText().toString() + " " + uporabnik.getId()+ " " + uporabnik.getUporabniskoIme() + " " + uporabnik.getPassword() + " "+ uporabnik.getEmail());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });

    }
}