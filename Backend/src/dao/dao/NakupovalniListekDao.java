package dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import vao.Izdelek;
import vao.Kosarica;
import vao.NakupovalniListek;
import vao.Trgovina;
import vao.TrgovinaIzdelek;
import vao.Uporabnik;
import dao.UporabnikDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class NakupovalniListekDao {
	public UporabnikDao uporabnikDao=new UporabnikDao();
	public Connection poveziBazo() {

        Connection conn = null;
        String url = "jdbc:mysql:///nakupovalniListek?cloudSqlInstance=august-gradient-351908:europe-west6:projects&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=test&password=test&allowPublicKeyRetrieval=TRUE";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url,"test","test");
            //System.out.println("POVEZAVA USPESNA");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
	
	//Dodajanje listka
	public void dodajNakupovalniListek(NakupovalniListek nakupovalniListek) throws SQLException {
		Connection conn = poveziBazo();
		
		String sql = "INSERT INTO NakupovalniListek (iUporabnik_idUporabnik, skupna_cena) VALUES (?,?, ?)";
        PreparedStatement prest=conn.prepareStatement(sql);
        prest.setInt(1, nakupovalniListek.getUporabnikID());
		prest.setFloat(2, (float) nakupovalniListek.getSkupnaCena());
		
		prest.executeUpdate();
		
		System.out.println("DODAJANJE LISTKA: " + nakupovalniListek.getId());
    }
	
	//Brisajne listka
	public void brisiNakupovalniListek(int id) throws SQLException {
		Connection conn = poveziBazo();
		
		String sql = "DELETE FROM NakupovalniListek WHERE idNakupovalniListek=?";
        PreparedStatement prest = conn.prepareStatement(sql);
        prest.setInt(1, id);
        
        prest.executeUpdate();
        System.out.println("BRISANJE LISTKA: " + id);
	}
	public double skupnaCenaListka(Trgovina tr) throws SQLException {
		Connection conn = poveziBazo();
		double cena=0;
		
		String sql= "select MIN(IzdelekVTrgovini.cena), COUNT(kolicina) from Kosarica,Izdelek,IzdelekVTrgovini"
				+ " where IzdelekID=idIzdelek"
				+ " and IzdelekID=Izdelek_idIzdelek and Izdelek_idIzdelek=idIzdelek"
				+ " and trgovina_idTrgovina=? group by idIzdelek";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,tr.getId());
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			double cenaIzdelka=result.getDouble(1)*result.getInt(2);
			cena+=cenaIzdelka;
		}
		System.out.println("Cena za celotno kosarico je: "+cena);
		
		return cena;
		
	}
	
	public double skupnaCenaListka(String nazivTrgovina) throws SQLException {
		Connection conn = poveziBazo();
		double cena=0;
		
		//s tem pridobim idTrgovine iz baze, ki ga potem v spodnjem sqlu uporabim za skupno ceno
		String sql1 = "select idTrgovina from Trgovina where naziv=?";
		PreparedStatement prest1= conn.prepareStatement(sql1);
		prest1.setString(1, nazivTrgovina);
		ResultSet result1 = prest1.executeQuery();
		int id = 0;
		if (result1.next()) {
            id = result1.getInt("idTrgovina");
            //System.out.println(count);
        }
		//System.out.println("ID" + id);
		
		String sql= "select MIN(IzdelekVTrgovini.cena), COUNT(kolicina) from Kosarica,Izdelek,IzdelekVTrgovini"
				+ " where IzdelekID=idIzdelek"
				+ " and IzdelekID=Izdelek_idIzdelek and Izdelek_idIzdelek=idIzdelek"
				+ " and trgovina_idTrgovina=? group by idIzdelek";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,id);
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			double cenaIzdelka=result.getDouble(1)*result.getInt(2);
			cena+=cenaIzdelka;
		}
		System.out.println("Cena za celotno kosarico v trgovini " + nazivTrgovina + " je: "+cena);
		
		return cena;
		
	}
	public Kosarica najcenejsaTrgovina() throws SQLException{
		Connection conn = poveziBazo();
		
		ArrayList<Double> arrCene = new ArrayList<Double>();
		ArrayList<String> arrNazivi = new ArrayList<String>();
		
		String sql= "select * from Trgovina group by Trgovina.idTrgovina";
		PreparedStatement prest= conn.prepareStatement(sql);
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			String naziv=result.getString("naziv");
			//skupnaCenaListka(naziv);
			arrCene.add(skupnaCenaListka(naziv));
			arrNazivi.add(naziv);
		}
		
		double najcenejsaCena=arrCene.get(0);
		String nazivTrgovine = "";
		for(int i=0; i<arrCene.size(); i++) {
			if (arrCene.get(i)<najcenejsaCena) {
				najcenejsaCena=arrCene.get(i);
				nazivTrgovine=arrNazivi.get(i);
			}
		}
		//System.out.println("Trgovina: " + naziv + " Cena: "+ najcenejsaCena);
		Kosarica najcenejsaKosarica = new Kosarica(nazivTrgovine, najcenejsaCena);
		return najcenejsaKosarica;
	}
	public ArrayList<String> najdiUporabniskeListke(String uporabniskoIme) throws SQLException {
		Connection conn = poveziBazo();
		UporabnikDao UD=new UporabnikDao();
		Uporabnik uporabnik=UD.najdiUporabnikID(uporabniskoIme);
		ArrayList<String> uporabniskeListke = new ArrayList<String>();
		
		String sql= "select * from nakupovalnilistek where Uporabnik_idUporabnik=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,uporabnik.getId());
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			String imeListka=result.getString("nazivListka");
			uporabniskeListke.add(imeListka);
		}
		return uporabniskeListke;
	}
	public ArrayList<String> najdiProdukteNaListku (String naziv) throws SQLException{
		Connection conn = poveziBazo();
		NakupovalniListek nakupovalniListek=this.najdiNakupovalniListek(naziv);
		ArrayList<String> produkteNaListku = new ArrayList<String>();
		
		String sql= "SELECT naziv from kosarica,nakupovalnilistek,izdelek WHERE"
				+ "idNakupovalniListek=nakupovalnilistek_idNakupovalniListek"
				+ "AND IzdelekID=idIzdelek"
				+ "and idNakupovalniListek=?;";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1,nakupovalniListek.getId());
		ResultSet result= prest.executeQuery();
		while(result.next()) {
			String imeProdukta=result.getString("naziv");
			produkteNaListku.add(imeProdukta);
		}
		return produkteNaListku;
	}
	public NakupovalniListek najdiNakupovalniListek(String naziv) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		NakupovalniListek vrni=null;
		String sql="select * from NakupovalniListek where nazivListka=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, naziv);
		ResultSet result=prest.executeQuery();
		 while (result.next()) {
		int id=result.getInt("idNakupovalniListek");
		int uporabnikID=result.getInt("Uporabnik_idUporabnik");
		double skupnaCena=result.getDouble("skupnaCena");
		String na= result.getString("nazivListka");
		NakupovalniListek nakupovalniListek=new NakupovalniListek(id,uporabnikID,skupnaCena,na);
		vrni=nakupovalniListek;
		 }
		return vrni;
	}
}