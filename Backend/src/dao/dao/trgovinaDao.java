package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import vao.Izdelek;
import vao.Trgovina;

public class trgovinaDao {
	
	public Connection poveziBazo() {

        Connection conn = null;
        String url = "jdbc:mysql:///nakupovalniListek?cloudSqlInstance=august-gradient-351908:europe-west6:projects&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=test&password=test&allowPublicKeyRetrieval=TRUE";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url,"test","test");
            //System.out.println("POVEZAVA USPESNA");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
	
	public void dodajTrgovino(Trgovina trgovina) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		 String sql = "INSERT INTO Trgovina (idTrgovina, naziv) VALUES (?,?)";
         PreparedStatement prest=conn.prepareStatement(sql);
         prest.setInt(1, trgovina.getId());
         prest.setString(2, trgovina.getNaziv());
		 prest.executeUpdate();
    }
	public Trgovina najdiTrgovina(String naziv) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		Trgovina vrni=null;
		String sql="select * from Trgovina where naziv=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, naziv);
		ResultSet result=prest.executeQuery();
		 while (result.next()) {
		int id=result.getInt("idTrgovina");
		String na= result.getString("naziv");
		Trgovina novaTrgovina=new Trgovina(id,na);
		vrni=novaTrgovina;
		 }
		 
		return vrni;
	}
}
