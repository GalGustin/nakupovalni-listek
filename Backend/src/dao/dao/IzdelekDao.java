package dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import vao.Izdelek;
import vao.NakupovalniListek;
import vao.Trgovina;
import vao.TrgovinaIzdelek;
import vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class IzdelekDao {
	
	public Connection poveziBazo() {

        Connection conn = null;
        String url = "jdbc:mysql:///nakupovalniListek?cloudSqlInstance=august-gradient-351908:europe-west6:projects&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=test&password=test&allowPublicKeyRetrieval=TRUE";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url,"test","test");
            //System.out.println("POVEZAVA USPESNA");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
	public void dodajIzdelek(Izdelek izdelek) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		 String sql = "INSERT INTO Izdelek (idIzdelek, naziv, opis) VALUES (?,?,?)";
         PreparedStatement prest=conn.prepareStatement(sql);
         prest.setInt(1, izdelek.getId());
         prest.setString(2, izdelek.getNaziv());
         prest.setString(3, izdelek.getOpis());
		 prest.executeUpdate();
    }
	public Izdelek najdiIzdelek(String barcode) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		Izdelek vrni=null;
		String sql="select * from Izdelek where barcode=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, barcode);
		ResultSet result=prest.executeQuery();
		 while (result.next()) {
		int id=result.getInt("idIzdelek");
		String na= result.getString("naziv");
		String opis= result.getString("opis");
		Izdelek novIzdelek=new Izdelek(id, barcode,na,opis);
		vrni=novIzdelek;
		 }
		 
		return vrni;
	}
	
	public boolean preveriIzdelek(String barcode) throws SQLException {
		Connection conn = poveziBazo();
		Statement st=conn.createStatement();
		Izdelek vrni=null;
		String sql="select * from Izdelek where barcode=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setString(1, barcode);
		ResultSet result=prest.executeQuery();
		 if (result.next()) {
			 return true;
			 //TODO --- Pregled izdelka(obstaja)
	}
		 else return false;
		 //TODO -- Dodaj izdelek(Ne obstaja)
	}
}


