package dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import vao.Izdelek;
import vao.NakupovalniListek;
import vao.Trgovina;
import vao.TrgovinaIzdelek;
import vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class UporabnikDao {
	
	public Connection poveziBazo() {

        Connection conn = null;
        String url = "jdbc:mysql:///nakupovalniListek?cloudSqlInstance=august-gradient-351908:europe-west6:projects&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=test&password=test&allowPublicKeyRetrieval=TRUE";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url,"test","test");
            //System.out.println("POVEZAVA USPESNA");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
	
	//dodajanje uporabnika, ce ne obstaja
	public void dodajUporabnika(Uporabnik uporabnik) throws SQLException {
		Connection conn = poveziBazo();
		if(najdiUporabnika(uporabnik.getUporabniskoIme())==false) {
			Statement st=conn.createStatement();
			st.executeUpdate("INSERT INTO Uporabnik (idUporabnik, uporabniskoIme, geslo, mail) "
                 +"VALUES ("+uporabnik.getId()+"," +"'"+uporabnik.getUporabniskoIme()+"'"  +", '"+ uporabnik.getEmail() +"'"  +", '"+uporabnik.getPassword()+ "')");
		}
	}
	
	//brisanje uporabnika
	public void brisiUporabnika(String uporabniskoIme) throws SQLException {
		Connection conn = poveziBazo();

        String sql = "DELETE FROM Uporabnik WHERE uporabniskoIme=?";
        PreparedStatement prest = conn.prepareStatement(sql);
        prest.setString(1, uporabniskoIme);
        
        int val = prest.executeUpdate();
        System.out.println("BRISANJEžUPORABNIKA: " +uporabniskoIme);
    }
	
	//preveri ce uporabnik obstaja in vrne true, ce obstaja ali false, ce ne obstaja
	public boolean najdiUporabnika(String uporabniskoIme)throws SQLException {
		System.out.println("ISKANJEžUPORABNIKA: " + uporabniskoIme);
		
		Connection conn = poveziBazo();
		String sql = "SELECT (uporabniskoIme) FROM Uporabnik WHERE uporabniskoIme=?";
		PreparedStatement prest=conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		prest.setString(1, uporabniskoIme);
		ResultSet rs = prest.executeQuery();
		rs.last();

	    if(rs.getRow() > 0) {
	    	return true;
	    }else {
	    	return false;
	    }   
    }
	public void mojIzdelek(Uporabnik uporabnik) throws SQLException {
		Connection conn = poveziBazo();
		String sql="create or replace view Izdelke as select naziv,opis,idIzdelek,uporabniskoime, COUNT(Kosarica.IzdelekID) as ProduktVKosarici from Kosarica,Izdelek,Uporabnik,NakupovalniListek where IzdelekID=idIzdelek and nakupovalnilistek_idnakupovalnilistek=idnakupovalnilistek and uporabnik_iduporabnik=iduporabnik\r\n"
				+ "and iduporabnik=?\r\n"
				+ "GROUP BY IzdelekID,idUporabnik;";
		String sql2="SELECT * from Izdelke where ProduktVKosarici = ( SELECT MAX(ProduktVKosarici) from Izdelke)";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, uporabnik.getId());;
		boolean rezultat=prest.execute();
		PreparedStatement prest2=conn.prepareStatement(sql2);
		ResultSet result=prest2.executeQuery();
		
		 if (result.next()) {
		int id=result.getInt("idIzdelek");
		String naziv= result.getString("naziv");
		String opis= result.getString("opis");
		int kolicina=result.getInt("ProduktVKosarici");
		System.out.println("Najbolj prodani produkt je: "+naziv+" Opis: "+opis+"\n in je bil na skupno: "+kolicina+"-- kosaric");
		 }
	}
}
