package dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import vao.Izdelek;
import vao.NakupovalniListek;
import vao.Trgovina;
import vao.TrgovinaIzdelek;
import vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class IzdelekTrgovinaDao {
	public IzdelekDao izdelekDao=new IzdelekDao();
	public trgovinaDao tDAO=new trgovinaDao();
	
	public Connection poveziBazo() {

        Connection conn = null;
        String url = "jdbc:mysql:///nakupovalniListek?cloudSqlInstance=august-gradient-351908:europe-west6:projects&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=test&password=test&allowPublicKeyRetrieval=TRUE";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url,"test","test");
            //System.out.println("POVEZAVA USPESNA");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
	
public void dodajIzdelekVTrgovini(TrgovinaIzdelek izdelek) throws SQLException {
	Connection conn = poveziBazo();
	Statement st=conn.createStatement();
	double povprecnaCena=this.povprecnaCenaVTrgovini(izdelek.getNaziv());
	if (izdelek.getCena()<5*povprecnaCena && izdelek.getCena()>0.2*povprecnaCena) {
	 String sql = "INSERT INTO IzdelekVTrgovini (cena, naziv, opis) VALUES (?,?,?)";
     PreparedStatement prest=conn.prepareStatement(sql);
     prest.setDouble(1, izdelek.getCena());
     prest.setString(2, izdelek.getNaziv());
     prest.setString(3, izdelek.getOpis());
	 prest.executeUpdate();
	}
}
	public void isciIzdelek(String barcode) throws SQLException {
		Connection conn = poveziBazo();
		String sql="select * from IzdelekVTrgovini where Izdelek_idIzdelek=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, izdelekDao.najdiIzdelek(barcode).getId());
		ResultSet result= prest.executeQuery();
		
		while (result.next()) {
			 String cena= result.getString("Cena");
			 System.out.println("Iskali ste ceno za izdelka: "+barcode);
			 System.out.println("Cena izdelka je: "+cena );
		 }
		 
		
	}
	public void isciIzdelek(int  id) throws SQLException {
		Connection conn = poveziBazo();
		String sql="select * from IzdelekVTrgovini where Izdelek_idIzdelek=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, id);
		ResultSet result= prest.executeQuery();
		
		if (result.next()) {
			 String cena= result.getString("Cena");
			 System.out.println("Iskali ste ceno za izdelka ki ima id: "+id);
			 System.out.println("Cena izdelka je: "+cena );
		 }
	}
	
	public void ceneIzdelkaVseTrgovine(String barcode) throws SQLException {
		Izdelek iskani= izdelekDao.najdiIzdelek(barcode);
		int idIzdelka=iskani.getId();
		Connection conn = poveziBazo();
		String sql="select * from IzdelekVTrgovini,Trgovina where Izdelek_idIzdelek=? and Trgovina_idTrgovina=idTrgovina";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, idIzdelka);
		ResultSet result= prest.executeQuery();
		
		while (result.next()) {
			 String cena= result.getString("Cena");
			 String nazivTrgovine=result.getString("Naziv");
			 System.out.println("Cena izdelka v "+nazivTrgovine+"  : "+cena );
		 }
	}
	
	public double ceneIzdelkaVTrgovino(String barcode,Trgovina tr) throws SQLException {
        Izdelek iskani= izdelekDao.najdiIzdelek(barcode);
        int idIzdelka=iskani.getId();
        Connection conn = poveziBazo();
        String sql="select cena from IzdelekVTrgovini where Izdelek_idIzdelek=? and Trgovina_idTrgovina=?";
        PreparedStatement prest= conn.prepareStatement(sql);
        //System.out.println("idIzdelka: " + idIzdelka + "  " + tr.getId());
        prest.setInt(1, idIzdelka);
        prest.setInt(2, tr.getId());
        ResultSet result= prest.executeQuery();
        double cenaVTrgovini = 0;
        if (result.next()) {
             double cena= result.getFloat("cena");
             cenaVTrgovini=cena;
        }
        System.out.println("Ceno ki se iskali v trgovini: "+tr.getNaziv()+" je : "+cenaVTrgovini);
        return cenaVTrgovini;
	}
	
	public void skupnaCenaVTrgovini(NakupovalniListek nl) throws SQLException {
	
		Connection conn = poveziBazo();
		String sql="select * from IzdelekVTrgovini,Trgovina,Kosarica where IzdelekVTrgovini_idTrgovinaIzdelek=idTrgovinaIzdelek and NakupovalniListek_idNakupovalniListek=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, nl.getId());
		ResultSet result= prest.executeQuery();
		
		double skupnaCena=0;
		while (result.next()) {
			 Double cena= result.getDouble("Cena");
			 double kolicina=result.getDouble("Kolicina");
			 skupnaCena+=cena*kolicina ;
			 String nazivTrgovine=result.getString("Naziv");
			 System.out.println("Cena izdelka v "+nazivTrgovine+"  : "+cena );
		 }
		 String nazivTrgovine=result.getString("Naziv");
		System.out.println("Skupna cena v "+nazivTrgovine+ " je: "+skupnaCena);
	}
	
	public void skupnaCenaVTrgovini(int id,Trgovina tr) throws SQLException {
		Connection conn = poveziBazo();
		String sql="select * from IzdelekVTrgovini,Kosarica "
				+ "		where Trgovina_idTrgovina=?"
				+ "		and NakupovalniListek_idNakupovalniListek=?";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, tr.getId());
		prest.setInt(2, id);
		ResultSet result= prest.executeQuery();
		
		double skupnaCena=0;
		String NT="";
		while (result.next()) {
			 Double cena= result.getDouble("Cena");
			 double kolicina=result.getDouble("Kolicina");
			 skupnaCena+=cena*kolicina ;
			
			 System.out.println("Cena izdelka  : "+cena );
			 System.out.println("Kolicina izdelka je: "+kolicina);
			 
		 }
		
		System.out.println("Skupna cena je: "+skupnaCena);
	}
	public double povprecnaCenaVTrgovini(String barcode) throws SQLException {
		Izdelek iskani= izdelekDao.najdiIzdelek(barcode);
		int idIzdelka=iskani.getId();
		Connection conn = poveziBazo();
		String sql="select * from IzdelekVTrgovini,Trgovina where Izdelek_idIzdelek=? and Trgovina_idTrgovina=idTrgovina";
		PreparedStatement prest= conn.prepareStatement(sql);
		prest.setInt(1, idIzdelka);
		ResultSet result= prest.executeQuery();
		double povprecnaCena=0;
		int stevec=0;
		double skupnaCena=0;
		
		while (result.next()) {
			 double cena= result.getDouble("Cena");
			 skupnaCena+=cena;
			 stevec++;
		 }
		povprecnaCena=skupnaCena/stevec;
		return povprecnaCena;
	}
	public void dodajIzTrgovino(String naziv, String nazivTrgovine,String barcode,double cena) throws SQLException {
		Connection conn = poveziBazo();
		Izdelek izdelek=izdelekDao.najdiIzdelek(barcode);
		Trgovina trgovina= tDAO.najdiTrgovina(nazivTrgovine);
		double povprecnaCena=this.povprecnaCenaVTrgovini(naziv);
		if (cena<5*povprecnaCena && cena>0.2*povprecnaCena) {
		 String sql = "INSERT INTO IzdelekVTrgovini (Trgovina_idTrgovina,Izdelek_idIzdelek,cena ) VALUES (?,?,?)";
	     PreparedStatement prest=conn.prepareStatement(sql);
	     prest.setInt(1,trgovina.getId());
	     prest.setInt(2, izdelek.getId());
	     prest.setDouble(3,cena);
		 prest.executeUpdate();
		}
	}
}