package dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;

import vao.Izdelek;
import vao.NakupovalniListek;
import vao.Trgovina;
import vao.TrgovinaIzdelek;
import vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.xml.transform.Result;

public class KosaricaDao {
	
	public Connection poveziBazo() {

        Connection conn = null;
        String url = "jdbc:mysql:///nakupovalniListek?cloudSqlInstance=august-gradient-351908:europe-west6:projects&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=test&password=test&allowPublicKeyRetrieval=TRUE";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url,"test","test");
            //System.out.println("POVEZAVA USPESNA");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
	
	public void najboljProdanIzdelek() throws SQLException {
		Connection conn = poveziBazo();
		String sql="create or replace view Izdelke as select naziv,opis,idIzdelek, COUNT(Kosarica.IzdelekID) as ProduktVKosarici"
				+ " from Kosarica,Izdelek "
				+ "where IzdelekID=idIzdelek "
				+ "GROUP BY IzdelekID;";
		String sql2="SELECT * from Izdelke where ProduktVKosarici = ( SELECT MAX(ProduktVKosarici) from Izdelke)";
		PreparedStatement prest= conn.prepareStatement(sql);
		boolean rezultat=prest.execute();
		PreparedStatement prest2=conn.prepareStatement(sql2);
		ResultSet result=prest2.executeQuery();
		
		 if (result.next()) {
		int id=result.getInt("idIzdelek");
		String naziv= result.getString("naziv");
		String opis= result.getString("opis");
		int kolicina=result.getInt("ProduktVKosarici");
		System.out.println("Najbolj prodani produkt je: "+naziv+" Opis: "+opis+"\n in je bil na skupno: "+kolicina+"-- kosaric");
		 }
	}
}

