package test_database;

import vao.Izdelek;
import vao.Kosarica;
import vao.NakupovalniListek;
import vao.Trgovina;
import vao.TrgovinaIzdelek;
import vao.Uporabnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

import dao.IzdelekDao;
import dao.IzdelekTrgovinaDao;
import dao.KosaricaDao;
import dao.NakupovalniListekDao;
import dao.UporabnikDao;
import dao.trgovinaDao;

public class main {
	
	public static void main(String[] args) throws SQLException {
		IzdelekDao dao=new IzdelekDao();
		trgovinaDao trgovinaDAO=new trgovinaDao();
		//Izdelek nov=new Izdelek(8,"Testiram","Opis ni dostopen");
		Trgovina lidl=new Trgovina(1,"Lidl");
		IzdelekTrgovinaDao iscemIzdelek= new IzdelekTrgovinaDao ();
		KosaricaDao kosaricaDao=new KosaricaDao();
		kosaricaDao.najboljProdanIzdelek();
		
		/*ustvarjanje testnega uporabnikDao in uporabnika
		UporabnikDao uporabnikDAO = new UporabnikDao();
		Uporabnik uporabnik1 = new Uporabnik(1,"uporabnik1", "uporabnik1Mail", "gesloUporabnik1");
		
		//ustvarjanje testnega nakupovalniListekDao
		NakupovalniListekDao nakupovalniListekDAO = new NakupovalniListekDao();
		NakupovalniListek nakupovalniListek1 = new NakupovalniListek(2, 18.50);
		System.out.println("Pridel sem do preverjanje!");
		double cena=nakupovalniListekDAO.skunaCenaListka(lidl);
		//iscemIzdelek.isciIzdelek("testni");
		//System.out.println("Iskanje deluje!");
		//trgovinaDAO.dodajTrgovino(novaTrgovina);
		//trgovinaDAO.dodajTrgovino(lidl);
		//dao.dodajIzdelek(nov);
		*/
		
		Connection conn = null;
	    try {
		      String url = "jdbc:mysql://localhost:3306/nakupovalnilistek";
		      //conn = DriverManager.getConnection(url, "root","");
		      //Izdelek izdelek1 = new Izdelek("voda", "pij");
		      //Izdelek Izdelek2= dao.najdiIzdelek("testni");
		      
		     //Trgovina iscemTrgovino=trgovinaDAO.najdiTrgovina("Mercator");
		     // System.out.println("Izdelek ki sem ga dobil je: "+Izdelek2.toString());
		      //System.out.println("Trgovino ki ste zahtevali  je: "+iscemTrgovino.toString());
		      //Statement stmt = null;
		      //String query = "select * from Trgovina";
		      //String test="INSERT INTO izdelek(idIzdelek,naziv,opis)"+" VALUES('1','superIzdelek','top izdelek')";
		      //System.out.println("Cena vode je "+ iscemIzdelek.ceneIzdelkaVTrgovino("Voda", lidl));
		     // iscemIzdelek.ceneIzdelkaVTrgovino("Voda", lidl);
		      
		      NakupovalniListekDao nakupovalniListekDAO = new NakupovalniListekDao();
		      Kosarica najcenejsaTrgovina=nakupovalniListekDAO.najcenejsaTrgovina();
		      System.out.println("Najcenejsa trgovina: " + najcenejsaTrgovina.getNaziv() + ", cena: " + najcenejsaTrgovina.getCena());
		      //Testiranje dodajanje in brisanje uporabniika
		      //uporabnikDAO.dodajUporabnika(uporabnik1);
		      //uporabnikDAO.brisiUporabnika("uporabnik1");
		      //uporabnikDAO.najdiUporabnika("uporabnik1");
		      
		      //Testiranje dodajanje in brisanje uporabnika
		      //nakupovalniListekDAO.dodajNakupovalniListek(nakupovalniListek1);
		      //nakupovalniListekDAO.brisiNakupovalniListek(3);
		    } catch (SQLException e) {
		        throw new Error("Problem", e);
		    } finally {
		      try {
		        if (conn != null) {
		            conn.close();
		        }
		      } catch (SQLException ex) {
		          System.out.println(ex.getMessage());
		      }
	    }
	}

}