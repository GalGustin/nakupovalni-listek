package vao;

public class Uporabnik {
	private int id;
	private String uporabniskoIme;
	private String password;
	private String email;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUporabniskoIme() {
		return uporabniskoIme;
	}
	public void setUporabniskoIme(String uporabniskoIme) {
		this.uporabniskoIme = uporabniskoIme;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Uporabnik(int id, String uporabniskoIme, String password, String email) {
		super();
		this.id = id;
		this.uporabniskoIme = uporabniskoIme;
		this.password = password;
		this.email = email;
	}

}
