package vao;

public class Trgovina {
	private int id;
	private String naziv;
	private Trgovina(String naziv) {
		super();
		this.naziv = naziv;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "Trgovina [id=" + id + ", naziv=" + naziv + "]";
	}
	public Trgovina(int id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
