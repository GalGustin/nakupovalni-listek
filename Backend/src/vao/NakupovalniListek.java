package vao;

public class NakupovalniListek {
	private int id;
	private int uporabnikID;
	private double skupnaCena;
	private String nazivListka;
	public String getNazivListka() {
		return nazivListka;
	}
	public void setNazivListka(String nazivListka) {
		this.nazivListka = nazivListka;
	}
	public NakupovalniListek(int uporabnikID, double skupnaCena, String nazivListka) {
		super();
		this.uporabnikID = uporabnikID;
		this.skupnaCena = skupnaCena;
		this.nazivListka = nazivListka;
	}
	public NakupovalniListek(int uporabnikID, double skupnaCena) {
		super();
		this.uporabnikID = uporabnikID;
		this.skupnaCena = skupnaCena;
	}
	@Override
	public String toString() {
		return "NakupovalniListek [id=" + id + ", uporabnikID=" + uporabnikID + ", skupnaCena=" + skupnaCena + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUporabnikID() {
		return uporabnikID;
	}
	public void setUporabnikID(int uporabnikID) {
		this.uporabnikID = uporabnikID;
	}
	public double getSkupnaCena() {
		return skupnaCena;
	}
	public void setSkupnaCena(double skupnaCena) {
		this.skupnaCena = skupnaCena;
	}
	
}

