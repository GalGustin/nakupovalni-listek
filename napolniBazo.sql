INSERT INTO `Trgovina` (`idTrgovina`, `naziv`) VALUES (NULL, 'Špar'), (NULL, 'Lidl'), (NULL, 'Hofer'), (NULL, 'Mercator'), (NULL, 'Tuš');
INSERT INTO `Izdelek`	(`idIzdelek`, `barcode`, `naziv`, `opis`) VALUES 
	(NULL, '4084500490581', 'Coca-Cola', 'Gazirana pijaca'),
	(NULL, '', 'Alpsko mleko', 'Za zajtrk'),
	(NULL, '', 'Magnezij', 'Pomaga pri regeneraciji'),
	(NULL, '3838999101319', 'Zala - Izvirska Voda', 'Osvežilna pijača'),
	(NULL, '3830055520478', 'Jana - naravna mineralna voda', 'Zdravi prigrizek'),
	(NULL, '', 'Banana', 'Zdravi prigrizek'),
	(NULL, '', 'Pomaranča', 'Zdravi prigrizek'),
	(NULL, '5060166695279', 'Monster Classic', 'Energijska pijača');
INSERT INTO `Uporabnik`	(`idUporabnik`, `uporabniskoIme`, `geslo`, `mail`) VALUES 
	(NULL, 'admin', 'admin', '44admin44@gmail.com'),
	(NULL, 'klemenk', 'klemenk', 'klemen.kuri@student.um.si'),
    (NULL, 'bojanp', 'bojanp', 'bojan.petrovski@student.um.si'),
    (NULL, 'galg', 'galg', 'gal.gustin@student.um.si');
INSERT INTO `IzdelekVTrgovini`	(`idTrgovinaIzdelek`, `Trgovina_idTrgovina`, `Izdelek_idIzdelek`, `cena`) VALUES 
	(NULL, 1, 1, 0.89),
	(NULL, 2, 1, 0.92),
    (NULL, 3, 1, 0.85),
    (NULL, 1, 4, 0.9);
INSERT INTO `NakupovalniListek`	(`idNakupovalniListek`, `listekNaziv`,`Uporabnik_idUporabnik`, `skupnaCena`) VALUES 
	(NULL, 'listek1',2, 0),
	(NULL, 'listek2',3, 0),
    (NULL, 'listek3',4, 0),
    (NULL, 'listek4',2, 0);
INSERT INTO `Kosarica`	(`NakupovalniListek_idNakupovalniListek`, `IzdelekID`, `kolicina`, `skupnaCenaKosarice`) VALUES 
	(1, 1, 2, 2.2),
	(1, 4, 1, 1.2),
	(1, 8, 1, 3.4),
    (2, 1, 3, 1.9),
    (2, 5, 1, 4.2);